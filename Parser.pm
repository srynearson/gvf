package GVF::Parser;
use Moose;
use Moose::Util::TypeConstraints;
use Carp;
use IO::File;
use 5.10.0;

with 'GVF::Roles';

our ($VERSION) = 0.01;

use Data::Dumper;


# run report write file IO.
my $write_fh = IO::File->new( ">>Report.txt" );


#-----------------------------------------------------------------------------
#------------------------------- Attributes ----------------------------------
#-----------------------------------------------------------------------------

subtype 'File'
    => as 'Str',
    => where { $_ =~ /(.*)\.gvf$/ },
    => message { croak "The file ($_) does not have a .gvf extension.\n\n" };

has 'gvf_file' => (
    is        => 'ro',
    isa       => 'File | FileHandle',
    required  => 1,
    trigger   => \&_build_pragma,
);

has 'feature_lines' => (
    is         => 'ro',
    isa        => 'Str | ArrayRef',
    predicate  => 'has_feature_line',
    lazy_build => 1,
);

around '_build_feature_lines' => sub {
    my ( $orig, $self ) = @_;
    
    my $dbfile = $self->get_dbfile;
    
    if ( -e $dbfile ){
        print $write_fh "\nUsed existing $dbfile file\n";
        return $dbfile;
    }
    else {
        print $write_fh "\nCreated a new sqlite file.\n";
        $self->$orig;
    }
};

#------------------------------------------------------------------------------
#------------------------------Methods-----------------------------------------
#------------------------------------------------------------------------------

sub _build_pragma {
    
    my $self = shift;
    my $pragma_line = $self->_file_splitter('pragma');

    my (@check_structured_pragma, @mis_formatted, @field_report );
    foreach my $lines( @$pragma_line ) {
    
        # clean up lines.
        chomp $lines;
        $lines =~ s/^#{1,}//g;
        $lines =~ s/^\s+//g;
        
        push @field_report, $lines;        
        #---------------------------------------------------------------
        # matches based on different version of pragmas of 1.06 GVF spec.
        #---------------------------------------------------------------
        if ($lines =~ /^(\w+)\s(.*)$/) {
            my $pragma = lc($1);
            my $value  = $2;
            
            my @single_term = qw/ population sex /;
            if ( $pragma ~~ @single_term){
                if ( $pragma eq 'population' ) { $self->set_population($value) };
                if ( $pragma eq 'sex' ) { $self->set_sex($value) };
            }
            else { push @check_structured_pragma, [$pragma, $value]; }
        }

        # capture all the versions of technology-platform
        elsif ( $lines =~ /^(technology-platform-(.*))\s(.*)$/) {
            my $pragma = lc($1);
            my $value = $3;
            
            if ( $value =~ /;{1,}/ ) { push @mis_formatted, $pragma; }

            if ( $pragma eq 'technology-platform-class' ) { $self->set_platform_class($value) };
            if ( $pragma eq 'technology-platform-name' ) { $self->set_platform_name($value) };
            if ( $pragma eq 'technology-platform-version' ) { $self->set_platform_version($value) };
        }
        elsif ( $lines =~ /^(technology-platform-(.*)-(.*))\s(.*)$/) {
            my $pragma = lc($1);
            my $value = $3;
            
            if ( $value =~ /;{1,}/ ) { push @mis_formatted, $pragma; }

            if ( $pragma eq 'technology-platform-machine-id' ) { $self->set_platform_machine_id($value) }; 
            if ( $pragma eq 'technology-platform-read-length' ) { $self->set_platform_read_length($value) }; 
            if ( $pragma eq 'technology-platform-read-type' ) { $self->set_platform_read_type($value) }; 
            if ( $pragma eq 'technology-platform-average-coverage' ) { $self->set_platform_average_coverage($value) }; 
        }
        elsif ( $lines =~ /^(technology-platform-(.*)-(.*)-(.*))\s(.*)$/) {
            my $pragma = lc($1);
            my $value = $3;
            
            if ( $value =~ /;{1,}/ ) { push @mis_formatted, $pragma; }
            if ( $pragma eq 'technology_platform_read_pair_span' ) { $self->set_platform_read_pair_span($value) }; 
        }
        
        # Capture the rest of the simple two word pragmas. 
        elsif ( $lines =~ /^((\w+)-(\w+))\s(.*)/) {
            my $pragma = lc($1);
            my $value  = $4;
            
            my @double_term =
                qw/ gvf-version reference-fasta feature-gff3 file-version
                    file-date sequencing-scope capture-regions sequence-alignment
                    variant-calling sample-description genomic-source multi-individual
                    feature-ontology genome-build gff-version/;
                                  
            if ( $pragma ~~ @double_term){
                if ( $value =~ /;{1,}/ ) { push @mis_formatted, $pragma; }
                    
                if ( $pragma eq 'gvf-version' ) { $self->set_gvf_version($value) };
                if ( $pragma eq 'reference-fasta' ) { $self->set_reference_fasta($value) };
                if ( $pragma eq 'feature-gff3' ) { $self->set_feature_gff3($value) };
                if ( $pragma eq 'file-version' ) { $self->set_file_version($value) };
                if ( $pragma eq 'file-date' ) { $self->set_file_date($value) };
                if ( $pragma eq 'sequencing-scope' ) { $self->set_sequencing_scope($value) };
                if ( $pragma eq 'capture-regions' ) { $self->set_capture_regions($value) };
                if ( $pragma eq 'sequence-alignment' ) { $self->set_sequence_alignment($value) };
                if ( $pragma eq 'variant-calling' ) { $self->set_variant_calling($value) };
                if ( $pragma eq 'sample-description' ) { $self->set_sample_description($value) };
                if ( $pragma eq 'genomic-source' ) { $self->set_genomic_source($value) };
                if ( $pragma eq 'multi-individual' ) { $self->set_multi_individual($value) };
                if ( $pragma eq 'feature-ontology' ) { $self->set_feature_ontology($value) };
                if ( $pragma eq 'genome-build' ) { $self->set_genome_build($value) };
                if ( $pragma eq 'gff-version' ) { $self->set_gff_version($value) };
            }
            else { push @check_structured_pragma, [$pragma, $value]; }            
        }
    }

    # send data to field report
    $self->set_field_report(\@field_report);
    
    # check if non match pragmas are in structured list.
    $self->_structured_pragmas(\@check_structured_pragma);
}   

#------------------------------------------------------------------------------

sub _build_feature_lines {
    
    my $self = shift;
    my $feature_line = $self->_file_splitter('feature');
    
    my ( @return_list, @attributes_check );
    foreach my $lines( @$feature_line ) {
        chomp $lines;
        
        my ($seq_id, $source, $type, $start, $end, $score, $strand, $phase, $attribute) = split(/\t/, $lines);
        my @attributes_list = split(/\;/, $attribute);
        
        # send attributes to utils to check support
        push @attributes_check, $attribute;
        
        my %atts;    
        foreach my $attributes (@attributes_list) {
            $attributes =~ /(.*)=(.*)/g;
            $atts{$1} = $2;   
        }
        
        my $feature = {
            seqid     => $seq_id,
            source    => $source,
            type      => $type,
            start     => $start,
            end       => $end,
            score     => $score,
            strand    => $strand,
            phase     => $phase,
            attribute => {
                %atts
            },
        };
        push @return_list, $feature;
    }
    
    # send data to writer.
    $self->database_add_features(\@return_list);
    
    # send attributes to check contend.
    $self->_attributes_check(\@attributes_check);
}

#------------------------------------------------------------------------------

sub _structured_pragmas {
    my ( $self, $pragma_list ) = @_;
    
    my @structured =
    qw/ individual-id source-method technology-platform data-source source-method phenotype-description /;
    
    my ( @unknown_pragmas, @region_set );
    foreach my $list ( @{$pragma_list} ) {
        chomp $list;
        
        if ( $list->[0] ~~ @structured ){
            
            # to split ";" containing lines.
            my @values = split(/;/, $list->[1]);

            my %content;
            foreach my $pairs (@values) {
                if ( $pairs =~ /(\w+)=(.*)/g ) {
                    $content{$1} = $2;
                }
            }
            
            if ( $list->[0] eq 'individual-id') { $self->set_technology_platform(\%content)};
            if ( $list->[0] eq 'source-method') { $self->set_source_method(\%content)};
            if ( $list->[0] eq 'technology-platform') { $self->set_technology_platform(\%content)};
            if ( $list->[0] eq 'data-source') { $self->set_data_source(\%content)};
            if ( $list->[0] eq 'source-method') { $self->set_source_method(\%content)};
            if ( $list->[0] eq 'phenotype-description') { $self->set_phenotype_description(\%content) };
                                                       
            # This will set each of the techology-platform pragmas for you.
            if ( $list->[0] eq 'technology-platform') {
                while (my ($key, $value) = each %content) {
                    if ( $key =~ /(read).(type)/i ){ $self->set_platform_read_type($value) };
                    if ( $key =~ /(Read).(pair).(span)/i ){ $self->set_platform_read_pair_span($value) };
                    if ( $key =~ /(Platform).(class)/i ){ $self->set_platform_class($value) };
                    if ( $key =~ /(Platform).(name)/i ){ $self->set_platform_name($value) };
                    if ( $key =~ /(Platform).(version)/i ){ $self->set_platform_version($value) };
                    if ( $key =~ /(Platform).(machine).(id)/i ){ $self->set_platform_machine_id($value) };
                    if ( $key =~ /(Platform).(read).(length)/i ){ $self->set_platform_read_length($value) };
                    if ( $key =~ /(Platform).(average).(coverage)/i ){ $self->set_platform_average_coverage($value) }
                    #else { $self->set_technology_platform($value) };
                }
            }
        }
        elsif ( $list->[0] eq 'sequence-region' ) {
            
            $list->[1] =~ /(.*)\s(\d+)\s(\d+)$/;
            my $chromosome = {
                chr   => $1,
                start => $2,
                end   => $3,
            };
            push @region_set, $chromosome;
        }
        else {
            push @unknown_pragmas, $list->[0]; 
        }
    }
    
    # set the sequence region
    $self->set_sequence_region(\@region_set);
}

#------------------------------------------------------------------------------


1;
