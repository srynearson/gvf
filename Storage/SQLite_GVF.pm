package GVF::Storage::SQLite_GVF;
use Moose::Role;
use Moose::Util::TypeConstraints;
use Carp;
use DBI;

use Data::Dumper;

#-----------------------------------------------------------------------------
#------------------------------- Attributes ----------------------------------
#-----------------------------------------------------------------------------

subtype 'SQLite_check'
    => as 'Str',
    => where { $_ !~ `sqlite3 -version` },
    => message { croak "sqlite3 required\n\n" }; 

has 'sqlite_file' => (
  is        => 'rw',
  isa       => 'Str',
  writer    => 'set_sqlite_file',
  reader    => 'get_sqlite_file',
  predicate => 'has_sqlite_file',
);

has 'dbh' => (
  is         => 'rw',
  isa        => 'Object',
  writer     => 'set_dbh',
  reader     => 'get_dbh',
  lazy_build => 1,
);

has 'dbfile' => (
  is         => 'rw',
  isa        => 'Str',
  reader     => 'get_dbfile',
  writer     => 'set_dbfile',
  lazy_build => 1,
);

#------------------------------------------------------------------------------
#----------------------------- Methods ----------------------------------------
#------------------------------------------------------------------------------

sub _build_dbfile {
  
  my $self = shift;

  my $dbfile = $self->get_file_name;
  $dbfile =~ s/(.*).gvf/$1.sqlite/g;
  $dbfile =~ s/GVF_Data/GVF_Data.sqlite/g;
  
  $self->set_dbfile($dbfile);
}

#-----------------------------------------------------------------------------

sub _build_dbh {

  my $self = shift;
  
  my $dbfile = $self->get_dbfile;

  my $dbh;
  if ( -e $dbfile ) {
    $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile","","");
  }
  else {
    $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile","","");
    $self->create_database;
  }

  $self->set_dbh($dbh);
}

#-----------------------------------------------------------------------------

sub create_database {
  
  my $self = shift;
  
  my $dbh = $self->get_dbh;
  
  $dbh->do("PRAGMA foreign_keys = ON");
  
  my $features = 
      'CREATE TABLE "FEATURES"(
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	"SeqId" VARCHAR(30) NOT NULL,
	"Source" VARCHAR(45) NOT NULL,
	"Type" VARCHAR(45) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Score" FLOAT,
	"Strand" VARCHAR(45)
      );';
      
  my $attributes = 
      'CREATE TABLE "ATTRIBUTES"(
	"ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	"AttributeID" VARCHAR(45),
	"Alias" VARCHAR(45),
	"DBxref" VARCHAR(45),
	"VariantSeq" VARCHAR(45),
	"ReferenceSeq" VARCHAR(45),
	"VariantReads" VARCHAR(45),
	"TotalReads" VARCHAR(45),
	"Zygosity" VARCHAR(45),
	"VariantFreq" VARCHAR(45),
	"VariantEffect" VARCHAR(45),
	"StartRange" VARCHAR(45),
	"EndRange" VARCHAR(45),
	"Phased" VARCHAR(45),
	"Genotype" VARCHAR(45),
	"Individual" VARCHAR(45),
	"VariantCodon" VARCHAR(45),
	"ReferenceCodon" VARCHAR(45),
	"VariantAA" VARCHAR(45),
	"BreakpointDetail" VARCHAR(45),
	"SequenceContext" VARCHAR(45),
	"FEATURES_ID" INTEGER NOT NULL,
	CONSTRAINT "fk_ATTRIBUTES_FEATURES"
	  FOREIGN KEY("FEATURES_ID")
	  REFERENCES "FEATURES"("ID")
      );';
      
  my $index = 'CREATE INDEX "ATTRIBUTES.fk_ATTRIBUTES_FEATURES" ON "ATTRIBUTES"("FEATURES_ID");';
  
  $dbh->do( $features ); 
  $dbh->do( $attributes ); 
  $dbh->do( $index ); 
  
  #$dbh->disconnect;
}  

#-----------------------------------------------------------------------------

sub drop_database {

  # Thanks Barry
  my $self = shift;
  my $sql_file = $self->get_sqlite_file;
  
  if ( -e $sql_file ){
    `rm $sql_file`;
  }
}  

#-----------------------------------------------------------------------------

sub database_add_features {
  my ( $self, $feature_list ) = @_;
  
  my $dbh = $self->get_dbh;
  
  foreach my $features ( @$feature_list){

    #load the FEATURE table.
    my $feature_handle = $dbh->prepare('INSERT INTO FEATURES (SeqId, Source, Type, Start, End, Score, Strand ) VALUES (?,?,?,?,?,?,?)');
    $feature_handle->execute(
      $features->{'seqid'},
      $features->{'source'},
      $features->{'type'},
      $features->{'start'},
      $features->{'end'},
      $features->{'score'},
      $features->{'strand'}
      );
    
    # Collect all the feature_id's for fk.
    my $feat_id_grab = $dbh->selectall_arrayref(
      "SELECT ID FROM FEATURES",
      #{ Slice => {} }
    );
    my $feature_id = @$feat_id_grab;

    
    #
    my $attributes_handle = $dbh->prepare(
    'INSERT INTO ATTRIBUTES (AttributeID, Alias, DBxref, VariantSeq, ReferenceSeq, VariantReads, TotalReads,
    Zygosity, VariantFreq, VariantEffect, StartRange, EndRange, Phased, Genotype,
    Individual, VariantCodon, ReferenceCodon, VariantAA, BreakpointDetail, SequenceContext, FEATURES_ID )
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
    
    $attributes_handle->execute(
      $features->{'attribute'}->{'ID'},
      $features->{'attribute'}->{'Alias'},
      $features->{'attribute'}->{'Dbxref'},
      $features->{'attribute'}->{'Variant_seq'},
      $features->{'attribute'}->{'Reference_seq'},
      $features->{'attribute'}->{'Variant_reads'},
      $features->{'attribute'}->{'Total_reads'},
      $features->{'attribute'}->{'Zygosity'},
      $features->{'attribute'}->{'Variant_freq'},
      $features->{'attribute'}->{'Variant_effect'},
      $features->{'attribute'}->{'Start_range'},
      $features->{'attribute'}->{'End_range'},
      $features->{'attribute'}->{'Phased'},
      $features->{'attribute'}->{'Genotype'},
      $features->{'attribute'}->{'Individual'},
      $features->{'attribute'}->{'Variant_codon'},
      $features->{'attribute'}->{'Reference_codon'},
      $features->{'attribute'}->{'Variant_aa'},
      $features->{'attribute'}->{'Breakpoint_detail'},
      $features->{'attribute'}->{'Sequence_context'},
      $feature_id,
    );
  }
}



  
1;  
