package GVF::Roles;
use Moose::Role;


# This is only a "role collector".


with ('GVF::Pragmas','GVF::Utils', 'GVF::Feature_Lines', 'GVF::Storage::SQLite_GVF');

1;