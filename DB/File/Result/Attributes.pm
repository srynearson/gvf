package GVF::DB::File::Result::Attributes;
use base qw/DBIx::Class::Core/;
use strict;
use warnings;


__PACKAGE__->table('ATTRIBUTES');

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "attributeid",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "alias",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "dbxref",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "variantseq",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "referenceseq",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "variantreads",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "totalreads",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "zygosity",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "variantfreq",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "varianteffect",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "startrange",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "endrange",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "phased",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "genotype",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "individual",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "variantcodon",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "referencecodon",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "variantaa",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "referenceaa",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "breakpointdetail",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "sequencecontext",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "features_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);


__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    "feature",
    'GVF::DB::File::Result::Features',
    { id => "features_id" },
    #{ is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

        


1;






