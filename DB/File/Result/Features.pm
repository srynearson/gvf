package GVF::DB::File::Result::Features;
use base qw/DBIx::Class::Core/;
use strict;
use warnings;


__PACKAGE__->table('FEATURES');

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "seqid",
  { data_type => "varchar", is_nullable => 0, size => 30 },
  "source",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "type",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "start",
  { data_type => "integer", is_nullable => 0 },
  "end",
  { data_type => "integer", is_nullable => 0 },
  "score",
  { data_type => "float", is_nullable => 1 },
  "strand",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);
 
# set the primary key
__PACKAGE__->set_primary_key('id');


# set relationships to other tables.
__PACKAGE__->has_many(
    'attributes' =>
    'GVF::DB::File::Result::Attributes',
    { "foreign.feature_id" => "self.id" },
    #{ cascade_copy => 0, cascade_delete => 0 },
);


1;

