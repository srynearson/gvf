package GVF::Utils;
use Moose::Role;
#use Moose::Util::TypeConstraints;
use Carp;
use feature ":5.10";
use File::Basename;

use Data::Dumper;

#------------------------------------------------------------------------------
#------------------------------Methods-----------------------------------------
#------------------------------------------------------------------------------

has 'filename' => (
    is         => 'rw',
    isa        => 'Str',
    writer     => 'set_file_name',
    reader     => 'get_file_name',
    lazy_build => 1,
);

#
#has 'pragmas_available' => (
#    is         => 'rw',
#    isa        => 'ArrayRef',
#    writer     => 'set_pragmas_available',
#    reader     => 'get_pragmas_available',
#    lazy_build => 1,
#);

#
#has 'request_list' => (
#    is      => 'rw',
#    isa     => 'ArrayRef',
#    builder => 'request_list_parser',
#);
#



#------------------------------------------------------------

sub _build_filename {
    
    my $self = shift;

    # CHANGE THIS TO USER GIVEN NAME.
    if ( ref($self->file) ){
        $self->set_file_name('GVF_Data')
    }
    else{
        my($filename, $directories, $suffix) = fileparse($self->file);
        $self->set_file_name($filename);
    }
}

#------------------------------------------------------------

sub file_splitter {

    my ( $self, $request ) = @_;    

    my $obj_fh;
    if ( ref($self->file) ){
        $obj_fh = $self->file || die "File" . $self->file . "can not be opened\n";
    }
    else{
        open ( $obj_fh, "<", $self->file) || die "File" . $self->file . "can not be opened\n";
    }

    my ( @pragma, @feature_line );
    foreach my $line ( <$obj_fh> ){
        chomp $line;
    
        $line =~ s/^\s+$//g;

        # captures pragma lines.        
        if ($line =~ /^#{1,}/) {
            push @pragma, $line;
        }
        # or feature_line
        else { push @feature_line, $line; }
    }
    $obj_fh->close;

    if ( $request eq 'pragma') { return \@pragma }
    if ( $request eq 'feature') { return \@feature_line }
}






#sub pragma_list_parser {
#
#    my $self = shift;
#
#    my @ids;
#    while (my ($keys, $values) = each %{ $self->{'pragma'}} ) {
#        push @ids, $keys;
#    }
#    return(\@ids);
#}

#------------------------------------------------------------


# this feature is not flushed out,
# and it just may be a way to view data.

sub _build_pragmas_available {

    my $self = shift;
    my @return_list;
    
    while (my ($keys, $values) = each %{ $self->{'pragma'}} ) {
        my $request = $values->[0];
        if (ref($request) eq 'HASH'){
            while (my ($k, $v) = each %{$request} ) {
                my $request_feeder = join( ':', $keys, $k) unless $keys eq 'sequence-region';
                push @return_list, $request_feeder;
            }
        }
    }
    return (\@return_list);
}

#------------------------------------------------------------





1;