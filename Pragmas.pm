package GVF::Pragmas;
use Moose::Role;
use Carp;
use 5.10.0;



# these methods will also capture GFF3 data
# as they are considered a simple pragma.

#-----------------------------------------------------------------------------
#------------------------------- Simple Pragmas ------------------------------
#-----------------------------------------------------------------------------

has 'gvf_version'   => (
    is        => 'rw',
    isa       => 'Value',
    writer    => 'set_gvf_version',
    reader    => 'get_gvf_version',
    predicate => 'has_gvf_version',
);

has 'gff_version' => (
    is     => 'rw',
    isa    => 'Value',
    writer => 'set_gff_version',
    reader => 'get_gff_version',
);

has 'reference_fasta' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_reference_fasta',
    reader => 'get_reference_fasta',
);

has 'feature_gff3' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_feature_gff3',
    reader => 'get_feature_gff3',
);

has 'file_version' => (
    is     => 'rw',
    isa    => 'Value',
    writer => 'set_file_version',
    reader => 'get_file_version',
);

has 'file_date' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_file_date',
    reader => 'get_file_date',
);

has 'individual_id' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_individual_id',
    reader => 'get_individual_id',
);

has 'population' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_population',
    reader => 'get_population',
);

has 'sex' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_sex',
    reader => 'get_sex', # gitty-gitty-goo
);

has 'technology_platform_class' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_platform_class',
    reader => 'get_platform_class',
);

has 'technology_platform_name' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_platform_name',
    reader => 'get_platform_name',
);

has 'technology_platform_version' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_platform_version',
    reader => 'get_platform_version',
);

has 'technology_platform_machine_id' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_platform_machine_id',
    reader => 'get_platform_machine_id',
);

has 'technology_platform_read_length' => (
    is     => 'rw',
    isa    => 'Int',
    writer => 'set_platform_read_length',
    reader => 'get_platform_read_length',
);

has 'technology_platform_read_type' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_platform_read_type',
    reader => 'get_platform_read_type',
);

has 'technology_platform_read_pair_span' => (
    is     => 'rw',
    isa    => 'Int',
    writer => 'set_platform_read_pair_span',
    reader => 'get_platform_read_pair_span',
);

has 'technology_platform_average_coverage' => (
    is     => 'rw',
    isa    => 'Int',
    writer => 'set_platform_average_coverage',
    reader => 'get_platform_average_coverage',
);

has 'sequencing_scope' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_sequencing_scope',
    reader => 'get_sequencing_scope',
);

has 'capture_method' => (
    is     => 'rw',
    isa    =>  'Str',
    writer => 'set_capture_method',
    reader => 'get_capture_method',
);

has 'capture_regions' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_capture_region',
    reader => 'get_capture_region',
);

has 'sequence_alignment' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_sequence_alignment',
    reader => 'get_sequence_aligment',
);

has 'variant_calling' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_variant_calling',
    reader => 'get_variant_calling',
);

has 'sample_description' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_sample_description',
    reader => 'get_sample_description',
);

has 'genomic_source' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_genomic_source',
    reader => 'get_genomic_source',
);

has 'multi_individual' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_multi_individual',
    reader => 'get_multi_individual',
);

has 'gff3_pragma' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_gff3_pragma',
    reader => 'get_gff3_pragma',
);

has 'feature_ontology' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_feature_ontology',
    reader => 'get_feature_ontology',
);

has 'genome_build' => (
    is     => 'rw',
    isa    => 'Str',
    writer => 'set_genome_build',
    reader => 'get_genome_build',
);


#-----------------------------------------------------------------------------
#--------------------------- Structured Pragmas ------------------------------
#-----------------------------------------------------------------------------


has 'technology_platform' => (
    is      => 'rw',
    isa     => 'HashRef', # maybe
    writer  => 'set_technology_platform',
    reader  => 'get_technology_platform',
);

has 'data_source' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_data_source',
    reader  => 'get_data_source',
);

has 'score_method' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_score_method',
    reader  => 'get_score_method',
);

has 'source_method' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_source_method',
    reader  => 'get_source_method',
);

has 'attribute_method' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_attribute_method',
    reader  => 'get_attribute_method',
);

has 'phenotype_description' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_phenotype_description',
    reader  => 'get_phenotype_description',
);

has 'phased_genotypes' => (
    is      => 'rw',
    isa     => 'HashRef',
    writer  => 'set_phased_genotypes',
    reader  => 'get_phased_genotypes',
);

has 'sequence_region' => (
    is     => 'rw',
    isa    => 'ArrayRef[HashRef]',
    writer => 'set_sequence_region',
    reader => 'get_sequence_region',
);





1;